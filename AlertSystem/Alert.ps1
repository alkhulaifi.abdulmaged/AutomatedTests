(GC index.html | SELECT-STRING '											<span>Failed</span>' -Context 3 ) | % { ($_.context.precontext)[0] } | Out-File tmp.txt


Clear-Content "ErrorTestFolders.txt"
# get all the folders of failed tests
$firstFound = 0
foreach($line in Get-Content tmp.txt) {
        # grab the test folder
        if (($line.Length -gt 0) -and ($firstFound -eq 0)){	
			$line -match '^.*href=''(.*)?'' file.*$'
			Echo $matches[1] >> ErrorTestFolders.txt
			$firstFound = 1
		}
}

# flirtage de mots techniques,  send all the folders of failed tests to the sharepoint
foreach($file in Get-Content ErrorTestFolders.txt) {
	
	$file = -join($file,"/index.html");
	# clear the file index from technical words
	(Get-Content $file) | ForEach-Object { $_ -replace "\/\/\*\[@text" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\]\]" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\[xpath\," } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\,\=concat\(" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\[\(\=" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\, \/\/\*\[contains\(@text\," } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\[\( \/\/\*\[contains\(@text\," } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\/\/\*\[contains\(@text\," } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\]\)\[[0-9]" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\[\[\(]*\/\/\*\[@class\=" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "concat\(" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace "\/\/\*\[id=" } > $file
	(Get-Content $file) | ForEach-Object { $_ -replace '@' } > $file
	(Get-Content $file) | out-file -encoding utf8  $file


	# retreive the name of the parent folder to upload it on sharepoint
	$ParentFolders = Split-Path -Path $file

# change the code of the page
	chcp 1252
	$FileDestination = [System.IO.File]::ReadAllText("path.txt", [System.Text.Encoding]::GetEncoding(1252))
	# CHCP 65001
	Copy-item -Force -Recurse -Verbose $ParentFolders -Destination $FileDestination

	$FileToChange = -join($FileDestination,"/",$ParentFolders,"/index.html");
	# change the extension from index.html to index.aspx
	Dir $FileToChange | rename-item -newname { [io.path]::ChangeExtension($_.name, "aspx") }
	
	$FileTo = -join("/",$ParentFolders,"/index.aspx");
	$url =-join($FileDestination,"\",$ParentFolders,"\index.html");
	Copy-item -Force -Recurse -Verbose $file -Destination $url
	
	#------------------------------------------------------------------------------------------
	# sending an email 
	$FileToSend = -join($FileDestination,"/",$ParentFolders,"/index.aspx");
	(GC $FileToSend | SELECT-STRING '		<title>' ) | Out-File tmp.txt
	Clear-Content "email.txt"

	foreach($line in Get-Content tmp.txt) {
			# grab the test title
			if ($line.Length -gt 0){	
				$line -match '^.*<title>(.*)?</title>.*$'
				Echo $matches[1] >> email.txt
			}
	}

	# print the failed step
	(GC $FileToSend | SELECT-STRING "																	<div class='badge failed'>" -Context 3 ) | % { ($_.context.precontext)[0] } | Out-File tmp.txt
	foreach($line in Get-Content tmp.txt) {

			# grab the test title
			if ($line.Length -gt 0){	
				$line -match '^.*<span>(.*)?</span>.*$'
				$matches[1] = -join('"',$matches[1],'"')
				$matches[1] >> email.txt
			}
	}

	# retreive the screenshot of the failed step
	(GC $FileToSend | SELECT-STRING "																	<div class='badge failed'>" -Context 8 ) | % { ($_.context.precontext)[0] } | Out-File tmp.txt
	foreach($line in Get-Content tmp.txt) {

			# grab the test title
			if ($line.Length -gt 0){	
				$line -match '^.*<img src=''(.*)?'' alt.*$'
				$matches[1] = -join($ParentFolders,'/',$matches[1])
				$matches[1] >> email.txt
			}
	}
	
	if ((Get-Content "email.txt") -ne $Null) {
		$i = 0
		$array = @(0) * 3
		foreach($line in Get-Content email.txt) {
			$array[$i] = $line
			$i++
		}
		
		$XYProcess = start-process "cmd.exe" "/k Java -jar SendMail.jar $($array[0]) $($array[1]) $($array[2]) $($FileTo)" -PassThru
		Start-Sleep -s 5
		Stop-Process $XYProcess.ID
	}
	
	Stop-Process -Name "cmd"
	Remove-Item tmp.txt
}


