@echo off
setlocal EnableDelayedExpansion
set "substring=Failed"
set lineNumber=0

for /f "delims=" %%L in (index.html) do (
	
	set "string=%%L"
	set tableauNoCamion[%lineNumber%]=%%L
	
	if "!string:%substring%=!"=="!string!" (
		echo (!tableauNoCamion[%lineNumber%]!
    )
	set /a lineNumber+=1
)
endlocal