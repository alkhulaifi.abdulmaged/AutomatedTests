import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import io.appium.java_client.TouchAction;
import java.util.Scanner;
import java.io.*;
import org.testng.ITestResult;

import org.testng.TestListenerAdapter; 
import org.testng.TestNG;
//package <set your test package>; 
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 

public class Test0000MonCookeoSeConnecteriAndroid{ 
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0000MonCookeoSeConnecteriAndroid";
    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        // dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "9DC7N17C14000833");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.groupeseb.moncookeo");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.groupeseb.cookeat.splashscreen.SplashScreenActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }


    @Test 
    public void testUntitled() {
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@text='Mon Univers']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}        
		driver.findElement(By.xpath("//*[@text='Connexion / Inscription']")).click();

		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@id='email']")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@id='email']")).sendKeys("aalkhulaifi.seb@gmail.com");
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@id='password']")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){}        		
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("a713153768");
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@text='Se connecter']")).click();
		try{Thread.sleep(5000);} catch(Exception ignore){} 
    } 
    @AfterMethod 
    public void tearDown(ITestResult result) { 
        driver.quit();
		String username = System.getProperty("user.name");
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
				
				// incrementer le compteur de erreurs détectés
				Scanner scanner = new Scanner( new File("C:\\Users\\"+username+"\\Desktop\\Tests\\Briques\\Codes\\alertAccounter.txt"));
				String text = scanner.useDelimiter("\\A").next();
				scanner.close();
				int alertAccounter= Integer.parseInt(text);
				System.out.println(" before :"+alertAccounter);
				alertAccounter++;
				System.out.println(" after :"+alertAccounter);
				// System.out.println(" detected errors: for session :"+username+" "+alertAccounter);
				if(alertAccounter == 3){
					System.out.println("\n on est dans la boucle if \n");
					alertAccounter = 0;
					Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\"+username+"\\appiumstudio-reports\\reports &&"+ 
					"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
				}
				BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\"+username+"\\Desktop\\Tests\\Briques\\Codes\\alertAccounter.txt"));
				writer.write(String.valueOf(alertAccounter));
				writer.close();
			}

		}
		catch (Exception e) 
        { 
            System.out.println("Something is going Wrong "); 
            e.printStackTrace(); 
        } 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){
// deviceId = args[0];
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test0000MonCookeoSeConnecteriAndroid.class });  
testng.addListener(tla);  
testng.run(); }  
}  
