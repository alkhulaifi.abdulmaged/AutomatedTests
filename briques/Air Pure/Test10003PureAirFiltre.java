import org.testng.TestListenerAdapter; 
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>; 
import io.appium.java_client.remote.AndroidMobileCapabilityType; 
import io.appium.java_client.android.AndroidDriver; 
import io.appium.java_client.android.AndroidElement; 
import io.appium.java_client.TouchAction; 
import java.time.Duration; 
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 
public class Test10003PureAirFiltre{ 
    private String reportDirectory = "reports"; 
    private String reportFormat = "xml"; 
    private String testName = "Test10003PureAirFiltre"; 
    protected AndroidDriver<AndroidElement> driver = null; 
    DesiredCapabilities dc = new DesiredCapabilities(); 

    @BeforeMethod 
    public void setUp() throws MalformedURLException { 
        dc.setCapability("reportDirectory", reportDirectory); 
        dc.setCapability("reportFormat", reportFormat); 
        dc.setCapability("testName", testName); 
        dc.setCapability(MobileCapabilityType.UDID, "6cb806c5"); 
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.groupeseb.airpurifier"); 
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.rihuisoft.loginmode.activity.AppStartActivity"); 
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc); 
        driver.setLogLevel(Level.INFO); 
    } 
    @Test 
    public void testUntitled() { 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@id='iv_more_flg']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Etat des filtres']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@contentDescription='Etat des filtres']")).click(); 
        new TouchAction(driver).press(66, 793).waitAction(Duration.ofMillis(697)).moveTo(74, 410).release().perform(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
		driver.findElement(By.xpath("//*[@contentDescription='Etat des filtres']")).click(); 
    } 
    @AfterMethod 
    public void tearDown(ITestResult result) { 
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
					Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports  && Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");  		
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){  
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test10003PureAirFiltre.class });  
testng.addListener(tla);  
testng.run(); }  
}  
