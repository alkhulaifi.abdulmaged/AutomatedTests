import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import io.appium.java_client.TouchAction;

import java.util.Scanner;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0000BodyPartnerSeConnecterIos{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0000BodyPartnerSeConnecterIos";
    protected IOSDriver<IOSElement> driver = null;
	private String pathAlertAccounter =" ";
    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "51027a64c37c5edea9c7a46d3c087ff79dfe536e");
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.body-partner");
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='CONNEXION / INSCRIPTION']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@class='UIATextField']")).click();
        driver.findElement(By.xpath("//*[@class='UIATextField']")).sendKeys("aalkhulaifi.seb@gmail.com");		 
        driver.findElement(By.xpath("//*[@text='Mot de passe']")).sendKeys("aalkhulaifi");		try{Thread.sleep(5000);} catch(Exception ignore){} 
		new TouchAction(driver).press(135, 598).waitAction(Duration.ofMillis(240)).moveTo(218, 306).release().perform();
        driver.findElement(By.xpath("//*[@text='Se connecter']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		
		final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:c:/**/*alertAccounter.txt");
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
				try {
					Files.walkFileTree(Paths.get("c:/Users"), new SimpleFileVisitor<Path>() {
						public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
							if (matcher.matches(file)) {
								pathAlertAccounter = file.toString();
							}
							return FileVisitResult.CONTINUE;
						}

						public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
							return FileVisitResult.CONTINUE;
						}
					});
				} catch (IOException e) {
					e.printStackTrace();
				}	
				
				
				Scanner scanner = new Scanner( new File(pathAlertAccounter));
				String text = scanner.useDelimiter("\\A").next();
				scanner.close(); // Put this call in a finally block
				//System.out.println(text);
				int alertAccounter= Integer.parseInt(text);
				alertAccounter++;
				if(alertAccounter == 3){
					//System.out.println(alertAccounter);
					alertAccounter = 0;
					Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\ititestdevnum\\appiumstudio-reports\\reports &&"+ 
					"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
				}
				BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\ititestdevnum\\Desktop\\Tests\\Briques\\codes\\alertAccounter.txt"));
				writer.write(String.valueOf(alertAccounter));
				writer.close();
			}

		}
		catch (Exception e) 
        { 
            System.out.println("Something is going Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0000BodyPartnerSeConnecterIos.class });
		testng.addListener(tla);
		testng.run(); }
}


