
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0003BodyPartnerSuiviIos{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0003BodyPartnerSuiviIos";
    protected IOSDriver<IOSElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "0c2ba4cedae81cb5180ffcba1b64323007e6bde6");
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.body-partner");
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@class='UIAStaticText' and ./parent::*[@text]]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Suivi']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Poids']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Masse maigre']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Masse maigre']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Masse grasse']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='3 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 	
        driver.findElement(By.xpath("//*[@text='1 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 	
        driver.findElement(By.xpath("//*[@text='1 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 		
        driver.findElement(By.xpath("//*[@text='1 sem.']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 		
        driver.findElement(By.xpath("//*[@text='1 sem.']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 		
        driver.findElement(By.xpath("//*[@text='1 an']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 	
        driver.findElement(By.xpath("//*[@text='1 an']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='3 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Masse grasse']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 		
        driver.findElement(By.xpath("//*[@text='Masse maigre']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Masse maigre']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 	
        driver.findElement(By.xpath("//*[@text='Poids']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 		
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
		Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0003BodyPartnerSuiviIos.class });
		testng.addListener(tla);
		testng.run(); }
}


