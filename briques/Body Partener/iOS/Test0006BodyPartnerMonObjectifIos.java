import io.appium.java_client.TouchAction; 
import java.time.Duration; 
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0006BodyPartnerMonObjectifIos{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0006BodyPartnerMonObjectifIos";
    protected IOSDriver<IOSElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "51027a64c37c5edea9c7a46d3c087ff79dfe536e");
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.body-partner");
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@class='UIAStaticText' and @height>0 and ./parent::*[@text]]")).click();		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Mon objectif' and (./preceding-sibling::* | ./following-sibling::*)[@text='menu_icone_objectif_off']]")).click();		try{Thread.sleep(3000);} catch(Exception ignore){} 		
		driver.findElement(By.xpath("//*[@text='D�marrer cet objectif' and (./preceding-sibling::* | ./following-sibling::*)[@text=concat('Reprendre l', \"'\", 'objectif commenc� le 22 juil. 2019.')]]")).click();
        driver.findElement(By.xpath("//*[@text=concat('Votre objectif : m', \"'\", 'affiner')]")).click();		try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='S�lectionnez des zones que vous aimeriez affiner :']")).click();		try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[6]/*[@class='UIAButton'])[6]")).click();
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[6]/*[@class='UIAButton'])[3]")).click();
        driver.findElement(By.xpath("//*[@text='Valider']")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est parti !')]")).click();
		
        new WebDriverWait(driver, 50).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='UIAStaticText' and @height>0 and ./parent::*[@text]]")));
        driver.findElement(By.xpath("//*[@class='UIAStaticText' and @height>0 and ./parent::*[@text]]")).click();
        driver.findElement(By.xpath("//*[@text='Mon objectif' and (./preceding-sibling::* | ./following-sibling::*)[@text='menu_icone_objectif_on']]")).click();		try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Modifier mon objectif']")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
		driver.findElement(By.xpath("//*[@text='D�marrer cet objectif' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='ME MUSCLER']]]")).click();
        driver.findElement(By.xpath("//*[@text='Votre objectif : me muscler']")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Votre objectif : me muscler']")).click();try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[6]/*[@class='UIAButton'])[6]")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[6]/*[@class='UIAButton'])[3]")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Valider']")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
                driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est parti !')]")).click();  try{Thread.sleep(3000);} catch(Exception ignore){} 
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='UIAStaticText' and @height>0 and ./parent::*[@text]]"))); try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@class='UIAStaticText' and @height>0 and ./parent::*[@text]]")).click(); try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Mon objectif' and (./preceding-sibling::* | ./following-sibling::*)[@text='menu_icone_objectif_on']]")).click();		try{Thread.sleep(3000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Modifier mon objectif']")).click(); try{Thread.sleep(3000);} catch(Exception ignore){}
		new TouchAction(driver).press(362, 1022).waitAction(Duration.ofMillis(232)).moveTo(408, 614).release().perform();
        driver.findElement(By.xpath("//*[@text=concat('Je n', \"'\", 'ai pas d', \"'\", 'objectif')]")).click();
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		// try
        // {
			// if (result.getStatus() == ITestResult.FAILURE) {
		// Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
// "Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			// }
		// }
		// catch (Exception e) 
        // { 
            // System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            // e.printStackTrace(); 
        // } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0006BodyPartnerMonObjectifIos.class });
		testng.addListener(tla);
		testng.run(); }
}


