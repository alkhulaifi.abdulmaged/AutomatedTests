
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.TouchAction;
import java.time.Duration;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0002BodyPartnerSilhouetteIos{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0002BodyPartnerSilhouetteIos";
    protected IOSDriver<IOSElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "0c2ba4cedae81cb5180ffcba1b64323007e6bde6");
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.body-partner");
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@class='UIAStaticText' and ./parent::*[@text]]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Silhouette']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='2 sem.']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[7]/*[@class='UIAStaticText' and @height>0])[1]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='1 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='6 mois']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView']]]/*[@class='UIAView'])[7]/*[@class='UIAStaticText' and @height>0])[1]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='graphs historique on']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("(//*[@class='UIATable' and ./parent::*[@class='UIAView']]/*[@class='UIAView' and @height>0 and ./*[@class='UIAView'] and ./*[@class='UIAStaticText']])[1]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        new TouchAction(driver).press(916, 376).waitAction(Duration.ofMillis(488)).moveTo(280,368).release().perform();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("(//*[@class='UIATable' and ./parent::*[@class='UIAWindow']]/*[@class='UIAView' and @height>0])[1]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
		driver.findElement(By.xpath("(//*[@class='UIATable' and ./parent::*[@class='UIAView']]/*[@class='UIAView' and @height>0 and ./*[@class='UIAView'] and ./*[@class='UIAStaticText']])[1]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 

	}

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
		Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0002BodyPartnerSilhouetteIos.class });
		testng.addListener(tla);
		testng.run(); }
}


