
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.TouchAction;
import java.time.Duration;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0001CakeFactorySeDeconnecterAndroid{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0001CakeFactorySeDeconnecterAndroid";
    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "ce01171153ca941f0c");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.groupeseb.cakefactory");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, 
"com.groupeseb.cookeat.splashscreen.SplashScreenActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='bottom_navigation_item_icon' and (./preceding-sibling::* |./following-sibling::*)[@text='Mon Univers']]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='ib_settings']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        new TouchAction(driver).press(903, 1603).waitAction(Duration.ofMillis(433)).moveTo(878,371).release().perform();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        new TouchAction(driver).press(910, 1628).waitAction(Duration.ofMillis(428)).moveTo(899,435).release().perform();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='Se déconnecter']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='SE DÉCONNECTER']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
		Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0001CakeFactorySeDeconnecterAndroid.class });
		testng.addListener(tla);
		testng.run(); }
}


