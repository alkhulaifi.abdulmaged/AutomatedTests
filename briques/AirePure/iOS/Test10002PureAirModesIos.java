
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test10002PureAirModesIos{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test10002PureAirModesIos";
    protected IOSDriver<IOSElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "51027a64c37c5edea9c7a46d3c087ff79dfe536e");
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.airpurifier");
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='btn open']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='btn close']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='ico daily nor']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        // driver.findElement(By.xpath("//*[@text='ico boost nor']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='ico mute nor']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='ico light high']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='ico light low']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        // driver.findElement(By.xpath("//*[@text='ico light low']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='ico light off']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
		new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='ico sleep nor']")));
        driver.findElement(By.xpath("//*[@text='ico sleep nor']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
		Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test10002PureAirModesIos.class });
		testng.addListener(tla);
		testng.run(); }
}


