import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import io.appium.java_client.android.AndroidKeyCode;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0000BodyPartenerSeConnecterAndroid{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0000BodyPartenerSeConnecterAndroid";
    protected AndroidDriver<AndroidElement> driver = null;
	String FileName = "" ;				
    final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:c:/**/*appiumstudio-reports/reports/index.html");

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
		dc.setCapability(MobileCapabilityType.UDID, "9DC7N17C14000833");
        dc.setCapability("autoDismissAlerts", "true");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.groupeseb.bodypartner");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".splashscreen.SplashScreenActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='CONNEXION / INSCRIPTION']")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='email']")).sendKeys("elobou83@gmail.com");		try{Thread.sleep(5000);} catch(Exception ignore){} 
		driver.findElement(By.xpath("//*[@class='android.webkit.WebView' and ./*[@id='LoginPage:j_id4']]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("elodieseb");		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.findElement(By.xpath("//*[@text='Se connecter']")).click();		try{Thread.sleep(8000);} catch(Exception ignore){} 
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			// pour relativiser les chemin de ficheirs, il faut déclarer le filename avec le matcher et ajouter la libraire nio aussi 
			if (result.getStatus() == ITestResult.FAILURE) {
				// get the relatif path of report file
				Files.walkFileTree(Paths.get("c:/Users"), new SimpleFileVisitor<Path>() {
				    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				        if (matcher.matches(file)) {
				            System.out.println("on est la \n");
				            FileName = file.getParent().toString();
				            System.out.println(FileName);
				        }
				        return FileVisitResult.CONTINUE;
				    }

				    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				        return FileVisitResult.CONTINUE;
				    }
				});
				//C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports
				Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd "+FileName+" &&"+ "Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0000BodyPartenerSeConnecterAndroid.class });
		testng.addListener(tla);
		testng.run(); }
}


