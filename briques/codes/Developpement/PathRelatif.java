import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class PathRelatif {
	
	public static void main(String arhs[]){

		// dont forget to add nio.jar to (create) the folder nio in C:/Developpement/nio
		final PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:c:/**/*alertAccounter.txt");
	    try {
			Files.walkFileTree(Paths.get("c:/Users"), new SimpleFileVisitor<Path>() {
			    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			        if (matcher.matches(file)) {
			            System.out.println(file);
			        }
			        return FileVisitResult.CONTINUE;
			    }

			    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
			        return FileVisitResult.CONTINUE;
			    }
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}


}
