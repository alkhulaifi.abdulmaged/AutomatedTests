& 'C:\Users\ititestdevnum\Desktop\Tests\briques\codes\listingDevises.bat'


Get-ChildItem . -Filter *.java | 
	Foreach-Object {
		
		 # accounting the connected devises ( Android for the moment)
		$devisesAccounter = 0
		$lines = Get-Content "C:\Users\ititestdevnum\Desktop\Tests\briques\codes\names.txt"
		if($_.FullName -Notlike '*Copy*'){
			
			foreach ($line in $lines)
			{
				if($line -Notlike '*List of devices*'){
					
					$id = ($line.Split(" "))[0]
					if($id.length -gt 4){
						
						$deviseId = -join('		dc.setCapability(MobileCapabilityType.UDID, "',$id,'");')
						$nameWithoutExetension = $_.FullName | % {[System.IO.Path]::GetFileNameWithoutExtension($_)}
						$nameFileWithoutPath = Split-Path $_.FullName -leaf
						
						# for the first devies, we execute the same test at the same terminal
						if($devisesAccounter -eq 0){
							
							$testContent = gc $nameFileWithoutPath
							foreach ($ligne in $testContent ) {
								if($ligne.contains('MobileCapabilityType.UDID') ){
									(gc $nameFileWithoutPath).replace($ligne, $deviseId) | Set-Content  $nameFileWithoutPath
								}
							}
							
							# complie and execute the same test
							$compilation = -join('/k Javac  -classpath "C:\Developpement\Appium5\java-client-5.0.0.jar;C:\Developpement\Appium5\java-client-5.0.0-javadoc.jar;C:\Developpement\okhttp-3.10.0\okhttp-3.10.0.jar;C:\Developpement\okhttp-3.10.0\okio-1.14.0.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\jcommander-1.48.jar;C:\Developpement\selenium-java-3.14.0\libs\httpcore-4.4.9.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\commons-logging-1.2.jar;C:\Developpement\ApacheCommonsIO\commons-io-2.6.jar;C:\Developpement\guava\guava-26.0-jre.jar;C:\Developpement\httpclient-4.5.4\httpclient-4.5.4.jar;C:\Developpement\selenium-java-3.14.0\client-combined-3.14.0.jar;C:\Developpement\testng-6.10\testng-6.10.jar;C:\Developpement\testng-6.10\testng-6.10-sources.jar;C:\Developpement\testng-6.10\testng-6.13-javadoc.jar;C:\Developpement\s-w3.14.0AllDepencies\selenium-java-3.14.0.jar;C:\Developpement\testng-6.10\org.testng.jar ;" ',$nameFileWithoutPath); 
							$execution = -join('Java  -classpath "C:\Developpement\Appium5\java-client-5.0.0.jar;C:\Developpement\Appium5\java-client-5.0.0-javadoc.jar;C:\Developpement\okhttp-3.10.0\okhttp-3.10.0.jar;C:\Developpement\okhttp-3.10.0\okio-1.14.0.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\jcommander-1.48.jar;C:\Developpement\selenium-java-3.14.0\libs\httpcore-4.4.9.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\commons-logging-1.2.jar;C:\Developpement\ApacheCommonsIO\commons-io-2.6.jar;C:\Developpement\guava\guava-26.0-jre.jar;C:\Developpement\httpclient-4.5.4\httpclient-4.5.4.jar;C:\Developpement\selenium-java-3.14.0\client-combined-3.14.0.jar;C:\Developpement\testng-6.10\testng-6.10.jar;C:\Developpement\testng-6.10\testng-6.10-sources.jar;C:\Developpement\testng-6.10\testng-6.13-javadoc.jar;C:\Developpement\s-w3.14.0AllDepencies\selenium-java-3.14.0.jar;C:\Developpement\testng-6.10\org.testng.jar ;" ',$nameWithoutExetension); 
							start-process "cmd.exe" "$($compilation) & $($execution)" -noNewWindow
						}else {
							# echo "devises accounter : " $devisesAccounter
							# echo "id : " $deviseId
							
							# in the case of many connected devises to the pc
							$fileCopy = -join($nameWithoutExetension,"Copy",$devisesAccounter,".java")
							$fileCopyWithoutExtension = $fileCopy | % {[System.IO.Path]::GetFileNameWithoutExtension($fileCopy)}
							Copy-Item $nameFileWithoutPath -Destination $fileCopy
							
							# modifiying the name of the copy class
							(gc $fileCopy).replace($nameWithoutExetension, $fileCopyWithoutExtension) | Set-Content  $fileCopy
							
							
							$copy = gc $fileCopy
							foreach ($ligne in $copy) {
								if($ligne.contains('MobileCapabilityType.UDID') ){
									(gc $fileCopy).replace($ligne, $deviseId) | Set-Content  $fileCopy
								}
							}
							
							# echo $fileCopy
							
							$compilation = -join('/k Javac  -classpath "C:\Developpement\Appium5\java-client-5.0.0.jar;C:\Developpement\Appium5\java-client-5.0.0-javadoc.jar;C:\Developpement\okhttp-3.10.0\okhttp-3.10.0.jar;C:\Developpement\okhttp-3.10.0\okio-1.14.0.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\jcommander-1.48.jar;C:\Developpement\selenium-java-3.14.0\libs\httpcore-4.4.9.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\commons-logging-1.2.jar;C:\Developpement\ApacheCommonsIO\commons-io-2.6.jar;C:\Developpement\guava\guava-26.0-jre.jar;C:\Developpement\httpclient-4.5.4\httpclient-4.5.4.jar;C:\Developpement\selenium-java-3.14.0\client-combined-3.14.0.jar;C:\Developpement\testng-6.10\testng-6.10.jar;C:\Developpement\testng-6.10\testng-6.10-sources.jar;C:\Developpement\testng-6.10\testng-6.13-javadoc.jar;C:\Developpement\s-w3.14.0AllDepencies\selenium-java-3.14.0.jar;C:\Developpement\testng-6.10\org.testng.jar ;" ',$fileCopy); 
							$execution = -join('Java  -classpath "C:\Developpement\Appium5\java-client-5.0.0.jar;C:\Developpement\Appium5\java-client-5.0.0-javadoc.jar;C:\Developpement\okhttp-3.10.0\okhttp-3.10.0.jar;C:\Developpement\okhttp-3.10.0\okio-1.14.0.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\jcommander-1.48.jar;C:\Developpement\selenium-java-3.14.0\libs\httpcore-4.4.9.jar;C:\Program Files (x86)\Experitest\AppiumStudio\clients\appium\java\commons-logging-1.2.jar;C:\Developpement\ApacheCommonsIO\commons-io-2.6.jar;C:\Developpement\guava\guava-26.0-jre.jar;C:\Developpement\httpclient-4.5.4\httpclient-4.5.4.jar;C:\Developpement\selenium-java-3.14.0\client-combined-3.14.0.jar;C:\Developpement\testng-6.10\testng-6.10.jar;C:\Developpement\testng-6.10\testng-6.10-sources.jar;C:\Developpement\testng-6.10\testng-6.13-javadoc.jar;C:\Developpement\s-w3.14.0AllDepencies\selenium-java-3.14.0.jar;C:\Developpement\testng-6.10\org.testng.jar ;" ',$fileCopyWithoutExtension); 
							
				
							start-process "cmd.exe" "$($compilation) & $($execution)" 
							start-process "cmd.exe" "$($execution)"

						}
						

						$devisesAccounter++
					}
				}
			}
		}
	}