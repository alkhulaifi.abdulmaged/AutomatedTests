
//package <set your test package>;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.android.AndroidKeyCode;

import java.io.*;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.time.Duration;

public class Test0001BodyPartenerTableauDeBordAndroid{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0001BodyPartenerTableauDeBordAndroid";
    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "9WV7N18224002361");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, 
"com.groupeseb.bodypartner");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, 
".splashscreen.SplashScreenActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

     public boolean isDisplayed(String targetResourceId) {
    	 
    	 boolean isElementPresent;
			try{

				MobileElement mobileElement = (MobileElement) driver.findElementByAndroidUIAutomator("new UiSelector().resourceId(\""+targetResourceId+"\")");
				WebDriverWait wait = new WebDriverWait(driver, 5);
				wait.until(ExpectedConditions.visibilityOf(mobileElement));
				isElementPresent = mobileElement.isDisplayed();
				return isElementPresent;	
			}catch(Exception e){
				isElementPresent = false;
				System.out.println(e.getMessage());
				return isElementPresent;
			} 
     }
    
    @Test
    public void testUntitled() {		
	
			try{Thread.sleep(7000);} catch(Exception ignore){} 
			
			
			
			if(isDisplayed("headerBurger")){
				
				driver.findElement(By.xpath("//*[@id='headerBurger']")).click();		try{Thread.sleep(3000);} catch(Exception ignore){}
			}
			else{
				
				// Enovyer le rapport
				driver.findElement(By.xpath("//*[@text=concat('Envoyer rapport d', \"'\", 'erreur')]")).click();
				try{Thread.sleep(3000);} catch(Exception ignore){}
				driver.findElement(By.xpath("//*[@id='icon' and ./parent::*[(./preceding-sibling::* | ./following-sibling::*)[@text='Gmail']]]")).click();
				driver.findElement(By.xpath("//*[@id='send']")).click();

				// se connecter si on est pas connecté
				try{Thread.sleep(5000);} catch(Exception ignore){} 
				driver.findElement(By.xpath("//*[@text='CONNEXION / INSCRIPTION']")).click();
				new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='email']")));
				
				driver.findElement(By.xpath("//*[@id='email']")).sendKeys("elobou83@gmail.com");		try{Thread.sleep(5000);} catch(Exception ignore){} 
				driver.findElement(By.xpath("//*[@class='android.webkit.WebView' and ./*[@id='LoginPage:j_id4']]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
				driver.findElement(By.xpath("//*[@id='password']")).sendKeys("elodieseb");		try{Thread.sleep(5000);} catch(Exception ignore){} 
				driver.pressKeyCode(AndroidKeyCode.BACK);
				driver.findElement(By.xpath("//*[@text='Se connecter']")).click();		try{Thread.sleep(8000);} catch(Exception ignore){} 
			}
        }

    @AfterMethod
    public void tearDown(ITestResult result) {
		
        driver.quit();
		// try
        // {
			// if (result.getStatus() == ITestResult.FAILURE) {
				// Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\ititestdevnum\\Desktop\\Tests\\auto\\c-5h\\connextion &&"+ "Run.bat\"");
			// }
		// }
		// catch (Exception e) 
        // { 
            // System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            // e.printStackTrace(); 
        // } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { 
Test0001BodyPartenerTableauDeBordAndroid.class });
		testng.addListener(tla);
		testng.run(); }
}


