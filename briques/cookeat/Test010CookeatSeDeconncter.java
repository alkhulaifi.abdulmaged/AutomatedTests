import org.testng.TestListenerAdapter; 
import org.testng.TestNG; 
//package <set your test package>; 
import io.appium.java_client.remote.IOSMobileCapabilityType; 
import io.appium.java_client.ios.IOSDriver; 
import io.appium.java_client.ios.IOSElement; 
import io.appium.java_client.TouchAction; 
import java.time.Duration; 
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 
public class Test010CookeatSeDeconncter{ 
    private String reportDirectory = "reports"; 
    private String reportFormat = "xml"; 
    private String testName = "Test010CookeatSeDeconncter"; 
    protected IOSDriver<IOSElement> driver = null; 
    DesiredCapabilities dc = new DesiredCapabilities(); 
 
    @BeforeMethod 
    public void setUp() throws MalformedURLException { 
        dc.setCapability("reportDirectory", reportDirectory); 
        dc.setCapability("reportFormat", reportFormat); 
        dc.setCapability("testName", testName); 
        dc.setCapability(MobileCapabilityType.UDID, "0c2ba4cedae81cb5180ffcba1b64323007e6bde6"); 
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.inhouse.cookeat"); 
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc); 
        driver.setLogLevel(Level.INFO); 
    } 
    @Test 
    public void testUntitled() { 
        driver.findElement(By.xpath("//*[@text='Mon Univers']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Paramètres']")).click(); 
        new TouchAction(driver).press(728, 2137).waitAction(Duration.ofMillis(796)).moveTo(728, 222).release().perform(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Se déconnecter' and @class='UIAStaticText']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Se déconnecter' and @class='UIAButton']")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){} 

    } 
    @AfterMethod 
    public void tearDown() { 
        driver.quit(); 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){  
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test010CookeatSeDeconncter.class });  
testng.addListener(tla);  
testng.run(); }  
}  
