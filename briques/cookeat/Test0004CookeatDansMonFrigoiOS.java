import org.testng.TestListenerAdapter; 
import org.testng.TestNG; 
//package <set your test package>; 
import io.appium.java_client.remote.IOSMobileCapabilityType; 
import io.appium.java_client.ios.IOSDriver; 
import io.appium.java_client.ios.IOSElement; 
import io.appium.java_client.TouchAction; 
import java.time.Duration; 
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 
public class Test0004CookeatDansMonFrigoiOS{ 
    private String reportDirectory = "reports"; 
    private String reportFormat = "xml"; 
    private String testName = "Test0004CookeatDansMonFrigoiOS"; 
    protected IOSDriver<IOSElement> driver = null; 
    DesiredCapabilities dc = new DesiredCapabilities(); 
    @BeforeMethod 
    public void setUp() throws MalformedURLException { 
        dc.setCapability("reportDirectory", reportDirectory); 
        dc.setCapability("reportFormat", reportFormat); 
        dc.setCapability("testName", testName); 
        dc.setCapability(MobileCapabilityType.UDID, "0c2ba4cedae81cb5180ffcba1b64323007e6bde6"); 
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.inhouse.cookeat"); 
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc); 
        driver.setLogLevel(Level.INFO); 
    } 
    @Test 
    public void testUntitled() { 
        // driver.findElement(By.xpath("//*[@text='Dans mon frigo']")).click(); 
        // new TouchAction(driver).press(85, 2071).waitAction(Duration.ofMillis(752)).moveTo(54, 1008).release().perform(); 
        // new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Les nouveautés de la communauté']"))); 
        // new WebDriverWait(driver, 100).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Les nouveautés de la communauté']"))); 
        // driver.findElement(By.xpath("//*[@text='Les nouveautés de la communauté']")).click();
		 new TouchAction(driver).press(85, 2071).waitAction(Duration.ofMillis(752)).moveTo(54, 1008).release().perform();
        try{Thread.sleep(5000);} catch(Exception ignore){}
		driver.findElement(By.xpath("//*[@text='Dans mon frigo']")).click(); 
		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Ajouter des ingrédients']")).click(); 
		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Beurre']")).click(); 
		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Bicarbonate de soude']")).click(); 
		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Supprimer']")).click(); 
    } 
    @AfterMethod 
    public void tearDown() { 
        driver.quit(); 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){  
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test0004CookeatDansMonFrigoiOS.class });  
testng.addListener(tla);  
testng.run(); }  
}  
