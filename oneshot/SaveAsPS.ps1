Add-Type -AssemblyName system.Windows.Forms
$saveFile = New-Object System.Windows.Forms.SaveFileDialog

$saveFile.Filter = "All Files (*.*)|*.*| Java (*.java)|*.java"
$saveFile.FilterIndex = 2
$saveFile.InitialDirectory = "C:\Users\adm-aalkhulaifi\Desktop\Tests\OneShot";
$saveFile.RestoreDirectory = $true

SET "param=%saveFile.FileName%"
$rc = $saveFile.ShowDialog()
if ($rc -eq [System.Windows.Forms.DialogResult]::OK)
{

	$libra = "import java.nio.file.FileSystems;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.FileVisitResult;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.Files;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.Path;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.PathMatcher;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.Paths;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.SimpleFileVisitor;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.nio.file.attribute.BasicFileAttributes;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.time.Duration;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import io.appium.java_client.TouchAction;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.util.Scanner;"
	$libra | Out-File -Append $saveFile.FileName
	

	$libra = "import org.testng.TestListenerAdapter;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import org.testng.TestNG;"
	$libra | Out-File -Append $saveFile.FileName

	$libra = "import org.testng.ITestResult;"
	$libra | Out-File -Append $saveFile.FileName
	
	$libra = "import java.io.*;"
	$libra | Out-File -Append $saveFile.FileName
	
	# get the clipboard content 
	powershell -sta "add-type -as System.Windows.Forms; [windows.forms.clipboard]::GetText()" >> $saveFile.FileName


	$FileNameWithoutPath = [System.IO.Path]::GetFileName($saveFile.FileName)
	$FileNameWithoutExtension =[System.IO.Path]::GetFileNameWithoutExtension($saveFile.FileName)

	# start-process "cmd.exe" "/k setup.bat $($saveFile.FileName)"
	# powershell -Command "Start-Sleep -s 4"
		
	# -----------------------------------------------------------------------		
	# pour afficher le nom de fichier sans l extenstion
		# powershell -Command "(echo $($saveFile.FileName) )  | Out-File -Encoding ASCII stdout.txt"
	# echo $FileNameWithoutExtension | Out-File -Encoding ASCII stdout.txt

	# -----------------------------------------------------------------------
	# rename the class name
	$TestTitle = -join("public class ",$FileNameWithoutExtension)
	(gc $FileNameWithoutPath) -replace 'public class Untitled ', $TestTitle | Out-File $FileNameWithoutPath

	#		Permutation: fileName -> tmp.java
	#					emptyFile -> fileName
	(gc $FileNameWithoutPath) | Out-File tmp.java
	Clear-Content $FileNameWithoutPath


	#		Supprimer la derniere ligne pour pouvoir ajouter et systeme d'alert et la methode main	
	(gc tmp.java )| Select -SkipLast 1 | out-File $FileNameWithoutPath	
	(gc $FileNameWithoutPath )| Select -SkipLast 1 | out-File $FileNameWithoutPath
	
	# Use regex to modify a value in parenthese
	$a= "public void tearDown()"
	$b = "public void tearDown(ITestResult result)"
	(Get-Content $FileNameWithoutPath) |Foreach-Object {$_ -replace [regex]::escape($a), $b }| Out-File $FileNameWithoutPath
	
	$libra = '		String username = System.getProperty("user.name");
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {'
	$libra | Out-File -Append $FileNameWithoutPath

	Write-Output  '		// incrementer le compteur de erreurs détectés
				Scanner scanner = new Scanner( new File("C:\\Users\\"+username+"\\Desktop\\Tests\\Briques\\Codes\\alertAccounter.txt"));
				String text = scanner.useDelimiter("\\A").next();
				scanner.close();
				int alertAccounter= Integer.parseInt(text);
				System.out.println(" before :"+alertAccounter);
				alertAccounter++;
				System.out.println(" after :"+alertAccounter);
				// System.out.println(" detected errors: for session :"+username+" "+alertAccounter);
				if(alertAccounter == 3){
					System.out.println("\n on est dans la boucle if \n");
					alertAccounter = 0;
				 Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\"+username 
					+"\\appiumstudio-reports\\reports && Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
				}
				BufferedWriter writer = new BufferedWriter(new FileWriter("C:\\Users\\"+username+"\\Desktop\\Tests\\Briques\\Codes\\alertAccounter.txt"));
				writer.write(String.valueOf(alertAccounter));
				writer.close();' | out-File $FileNameWithoutPath -append

	
	$libra = '			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } '
	$libra | Out-File -Append $FileNameWithoutPath
	

	# Edit report title
	$TestTitle = -join('    private String testName = "',$FileNameWithoutExtension,'"')
	(gc $saveFile.FileName).replace('    private String testName = "Untitled"', $TestTitle) | Set-Content  $saveFile.FileName

	# -----------------------------------------------------------------------
	# ajouter la methode main 
	
	Add-Content $FileNameWithoutPath '	@SuppressWarnings("deprecation")'
	Add-Content $FileNameWithoutPath '	public static void main (String [] args){' 
	Add-Content $FileNameWithoutPath '		TestListenerAdapter tla = new TestListenerAdapter();' 
	Add-Content $FileNameWithoutPath '		TestNG testng = new TestNG();'
	Add-Content $FileNameWithoutPath "		testng.setTestClasses(new Class[] { $($FileNameWithoutExtension).class });" 
	Add-Content $FileNameWithoutPath '		testng.addListener(tla);' 
	Add-Content $FileNameWithoutPath '		testng.run(); }'  
	Add-Content $FileNameWithoutPath '}' 

	# -----------------------------------------------------------------------
	# supprimer le trace de echo off 
	(gc $FileNameWithoutPath) -replace 'ECHO is off.', ' ' | Out-File $FileNameWithoutPath
	(gc $FileNameWithoutPath ) | select-string -pattern 'ECHO' -notmatch | Out-File  $FileNameWithoutPath
	
	# -----------------------------------------------------------------------
	$t= 0
	foreach($line in Get-Content $FileNameWithoutPath) {
		if($line -like '*testUntitled(*' -Or $t -eq 1){

			$replace = '		try{Thread.sleep(3000);} catch(Exception ignore){} '
			if($line -Notlike '*?}*'){
				(get-content $FileNameWithoutPath).replace($line, $line+$replace) | set-content $FileNameWithoutPath
			}
			$t =1
			if($line -like '*?}*'){
				$t =0
			}
		}
	}
	
	# change encoding format to utf8
	# $MyFile = Get-Content $FileNameWithoutPath
	# [IO.File]::WriteAllLines($FileNameWithoutPath, $MyFile)
	Remove-item tmp.java

}