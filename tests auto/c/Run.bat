@echo off
setlocal enableDelayedExpansion
	REM pour plus de lisibilité, les bibliothèques seront découpé en strings de deux bibliothèques par string

SET "lib1=C:\Developpement\Appium5\java-client-5.0.0.jar;C:\Developpement\Appium5\java-client-5.0.0-javadoc.jar;"
SET "lib2=C:\Developpement\okhttp-3.10.0\okhttp-3.10.0.jar;C:\Developpement\okhttp-3.10.0\okio-1.14.0.jar;"
SET "lib3=C:\Developpement\jcommander-1.48.jar;C:\Developpement\selenium-java-3.14.0\libs\httpcore-4.4.9.jar;"
SET "lib4=C:\Developpement\commons-logging-1.2.jar;C:\Developpement\ApacheCommonsIO\commons-io-2.6.jar;"
SET "lib5=C:\Developpement\guava\guava-26.0-jre.jar;C:\Developpement\httpclient-4.5.4\httpclient-4.5.4.jar;"
SET "lib6=C:\Developpement\selenium-java-3.14.0\client-combined-3.14.0.jar;C:\Developpement\testng-6.10\testng-6.10.jar;"
SET "pat=%lib1%%lib2%%lib3%%lib4%%lib5%%lib6%"

FOR /L %%A IN (1,0,10) DO (

	for %%f in (*.java ^| sort) do ( 


		Javac  -classpath %pat% "%%~f" 
		java -classpath %pat% "%%~nf" 
		ping -n 9 localhost > nul )
		del "*.class"
		TIMEOUT /T 3600
	)

)