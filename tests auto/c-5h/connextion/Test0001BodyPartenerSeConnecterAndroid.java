import io.appium.java_client.android.AndroidKeyCode;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.ITestResult;
import java.io.*;
//package <set your test package>;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import org.testng.annotations.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Test0001BodyPartenerSeConnecterAndroid{
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Test0001BodyPartenerSeConnecterAndroid";
    protected AndroidDriver<AndroidElement> driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();
    
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "9WV7N18224002361");
        dc.setCapability("autoDismissAlerts", "true");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.groupeseb.bodypartner");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".splashscreen.SplashScreenActivity");
        driver = new AndroidDriver<>(new URL("http://localhost:4725/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {		
		try{Thread.sleep(7000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@text='CONNEXION / INSCRIPTION']")).click();		try{Thread.sleep(7000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='email']")).sendKeys("elobou83@gmail.com");		try{Thread.sleep(5000);} catch(Exception ignore){} 
		driver.findElement(By.xpath("//*[@class='android.webkit.WebView' and ./*[@id='LoginPage:j_id4']]")).click();		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.findElement(By.xpath("//*[@id='password']")).sendKeys("elodieseb");		try{Thread.sleep(5000);} catch(Exception ignore){} 
        driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.findElement(By.xpath("//*[@text='Se connecter']")).click();		try{Thread.sleep(8000);} catch(Exception ignore){} 
    }

    @AfterMethod
    public void tearDown(ITestResult result) {
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
		Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\aalkhulaifi\\appiumstudio-reports\\reports &&"+ 
"Powershell.exe -executionpolicy Bypass -File Alert.ps1\"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
	@SuppressWarnings("deprecation")
	public static void main (String [] args){
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { Test0001BodyPartenerSeConnecterAndroid.class });
		testng.addListener(tla);
		testng.run(); }
}


