import org.testng.TestListenerAdapter; 
import org.testng.TestNG; 
//package <set your test package>; 
import io.appium.java_client.remote.IOSMobileCapabilityType; 
import io.appium.java_client.ios.IOSDriver; 
import io.appium.java_client.ios.IOSElement; 
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 
public class Test0003MonCookeoStepByStepiOS{ 
    private String reportDirectory = "reports"; 
    private String reportFormat = "xml"; 
    private String testName = "Test0003MonCookeoStepByStepiOS"; 
    protected IOSDriver<IOSElement> driver = null; 
    DesiredCapabilities dc = new DesiredCapabilities(); 
	
    @BeforeMethod 
    public void setUp() throws MalformedURLException { 
        dc.setCapability("reportDirectory", reportDirectory); 
        dc.setCapability("reportFormat", reportFormat); 
        dc.setCapability("testName", testName); 
        dc.setCapability(MobileCapabilityType.UDID, "51027a64c37c5edea9c7a46d3c087ff79dfe536e"); 
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.MonCookeo"); 
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc); 
        driver.setLogLevel(Level.INFO); 
    } 
    @Test 
    public void testUntitled() { 
		
		try{Thread.sleep(3000);} catch(Exception ignore){}
		driver.findElement(By.xpath("//*[@text='Recherche']")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){}
		driver.findElement(By.xpath(" //*[contains(@text,'ai envie de')]")).click();
		driver.findElement(By.xpath(" //*[contains(@text,'ai envie de')]")).sendKeys("Poulet au curry");
		
        // driver.findElement(By.xpath("//*[@text='Search']")).click();
		try{Thread.sleep(5000);} catch(Exception ignore){}
        driver.findElement(By.xpath("//*[@text='Poulet au curry']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}
        // driver.findElement(By.xpath("//*[@text='recipes_detail_recipe_does_not_belong_to_notebook_accessibility_traits_ios']")).click(); 
		// try{Thread.sleep(3000);} catch(Exception ignore){}
        // driver.findElement(By.xpath("//*[@text='Connectez-vous pour profiter de toutes les fonctionnalités (favoris, commentaires, notes personnelles...)']")).click(); 
		
		driver.findElement(By.xpath("//*[@text='Démarrer la recette']")).click();
        try{Thread.sleep(3000);} catch(Exception ignore){}
		// driver.findElement(By.xpath("//*[@text='Plus tard']")).click();
		// driver.findElement(By.xpath("//*[@text='Fermer']")).click();
		
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text=concat('C', \"'\", 'est fait !')]")).click();
        driver.findElement(By.xpath("//*[@text='Terminer la recette']")).click();

		
		try{Thread.sleep(3000);} catch(Exception ignore){} 

    } 
    @AfterMethod 
    public void tearDown() { 
        driver.quit(); 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){  
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test0003MonCookeoStepByStepiOS.class });  
testng.addListener(tla);  
testng.run(); }  
}  
