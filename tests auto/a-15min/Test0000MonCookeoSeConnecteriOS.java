import java.io.*;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter; 
import org.testng.TestNG;
//package <set your test package>; 
import io.appium.java_client.remote.IOSMobileCapabilityType; 
import io.appium.java_client.ios.IOSDriver; 
import io.appium.java_client.ios.IOSElement; 
import io.appium.java_client.remote.MobileCapabilityType; 
import org.openqa.selenium.support.ui.ExpectedConditions; 
import org.openqa.selenium.support.ui.WebDriverWait; 
import org.openqa.selenium.remote.DesiredCapabilities; 
import org.openqa.selenium.By; 
import org.testng.annotations.*; 
import java.net.URL; 
import java.net.MalformedURLException; 
import java.util.logging.Level; 
public class Test0000MonCookeoSeConnecteriOS{ 
    private String reportDirectory = "reports"; 
    private String reportFormat = "xml"; 
    private String testName = "Test0000MonCookeoSeConnecteriOS"; 
    static String deviceId;
	protected IOSDriver<IOSElement> driver = null; 
	
    DesiredCapabilities dc = new DesiredCapabilities(); 
 
    @BeforeMethod 
    public void setUp() throws MalformedURLException { 
        dc.setCapability("reportDirectory", reportDirectory); 
        dc.setCapability("reportFormat", reportFormat); 
        dc.setCapability("testName", testName); 
        dc.setCapability(MobileCapabilityType.UDID, "51027a64c37c5edea9c7a46d3c087ff79dfe536e"); 
        dc.setCapability(IOSMobileCapabilityType.BUNDLE_ID, "com.groupeseb.MonCookeo"); 
        driver = new IOSDriver<>(new URL("http://localhost:4725/wd/hub"), dc); 
//		driver.resetApp();
        driver.setLogLevel(Level.INFO); 
    } 
    @Test 
    public void testUntitled() {
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@text='Mon Univers']")).click(); 
		try{Thread.sleep(3000);} catch(Exception ignore){}        
		driver.findElement(By.xpath("//*[@text='Connexion / Inscription']")).click();

		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@class='UIATextField']")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@class='UIATextField']")).sendKeys("aalkhulaifi.seb@gmail.com");
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAWebView']]]]]]/*/*[@class='UIAView' and ./parent::*[@class='UIAView']])[8]/*[@class='UIAView'])[2]")).click();
		try{Thread.sleep(3000);} catch(Exception ignore){}        		
        driver.findElement(By.xpath("((//*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAView' and ./parent::*[@class='UIAWebView']]]]]]/*/*[@class='UIAView' and ./parent::*[@class='UIAView']])[8]/*[@class='UIAView'])[2]")).sendKeys("a713153768");
		
		try{Thread.sleep(3000);} catch(Exception ignore){}        
        driver.findElement(By.xpath("//*[@text='Se connecter']")).click();
		try{Thread.sleep(5000);} catch(Exception ignore){} 
    } 
    @AfterMethod 
    public void tearDown(ITestResult result) { 
        driver.quit();
		try
        {
			if (result.getStatus() == ITestResult.FAILURE) {
					Runtime.getRuntime().exec("cmd /c start cmd.exe /K \"cd C:\\Users\\ititestdevnum\\appiumstudio-reports\\reports  && Powershell.exe -executionpolicy Bypass -File Alert.ps1 "+System.getProperty("user.dir").toString()+"");
			}
		}
		catch (Exception e) 
        { 
            System.out.println("HEY Buddy ! U r Doing Something Wrong "); 
            e.printStackTrace(); 
        } 
    } 
@SuppressWarnings("deprecation") 
public static void main (String [] args){
// deviceId = args[0];
TestListenerAdapter tla = new TestListenerAdapter();  
TestNG testng = new TestNG();  
testng.setTestClasses(new Class[] { Test0000MonCookeoSeConnecteriOS.class });  
testng.addListener(tla);  
testng.run(); }  
}  
